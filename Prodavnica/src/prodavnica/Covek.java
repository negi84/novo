package prodavnica;

public class Covek {
	private String ime;
	private String prezime;
	private String nadimak;
	private Integer godine;
	private Integer stepenStrucneSpreme;
	private Grad mestoStanovanja;
	
	public Covek() {
		super();
	}
	
	public String getPotpis(){
		return ime + ", " + prezime;
	}
	
	public Double getKoeficijent(int umnozak){
		return stepenStrucneSpreme / 0.78 * godine * umnozak;
	}
	
	public String getMestoStanovanja(Boolean euro){
		String dr = mestoStanovanja.getDrzava();
		if(euro = true){
			return mestoStanovanja + ", " + dr;
			}else{
				return dr + ", " + mestoStanovanja;
			}
			
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getNadimak() {
		return nadimak;
	}

	public void setNadimak(String nadimak) {
		this.nadimak = nadimak;
	}

	public Integer getGodine() {
		return godine;
	}

	public void setGodine(Integer godine) {
		this.godine = godine;
	}

	public Integer getStepenStrucneSpreme() {
		return stepenStrucneSpreme;
	}

	public void setStepenStrucneSpreme(Integer stepenStrucneSpreme) {
		this.stepenStrucneSpreme = stepenStrucneSpreme;
	}

	public Grad getMestoStanovanja() {
		return mestoStanovanja;
	}

	public void setMestoStanovanja(Grad mestoStanovanja) {
		this.mestoStanovanja = mestoStanovanja;
	}
	
	

}
