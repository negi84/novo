package prodavnica;

import java.util.ArrayList;

public class Adresa {
	private String ulica;
	private Integer broj;
	private Grad grad;
	private Fabrika fabrikaNaAdresi;
	private Prodavnica prodavnicaNaAdresi;

	public Adresa() {
		super();
	}

	public ArrayList<Proizvod> getProizvodiFabrike() {
		return fabrikaNaAdresi.getProizvodi();
	}
	
	public ArrayList<Double> getUkupneCeneProizvoda(){
		ArrayList<Double>rez = new ArrayList<Double>();
		ArrayList<Proizvod>sviProizvodi = prodavnicaNaAdresi.getProizvodiUPonudi();
		for(Proizvod i : sviProizvodi){
			rez.add(i.getUkupnaVrednost());
		}
		return rez;
	} 
	
	public Double getEkstrem(Boolean minMax){
		
		Double najskupljiProizvod = 0.0;
		
		if(minMax){
			for (Proizvod p : prodavnicaNaAdresi.getProizvodiUPonudi()) {
				if(p.getUkupnaVrednost() > najskupljiProizvod){
					najskupljiProizvod = p.getUkupnaVrednost();
			}
		
		}
		}
		
		if(!minMax ){
			for (Proizvod p : prodavnicaNaAdresi.getProizvodiUPonudi()) {
				if(p.getUkupnaVrednost() < najskupljiProizvod){
					najskupljiProizvod = p.getUkupnaVrednost();
			}
			}
			
		}
		
		return najskupljiProizvod;
		
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public Integer getBroj() {
		return broj;
	}

	public void setBroj(Integer broj) {
		this.broj = broj;
	}

	public Grad getGrad() {
		return grad;
	}

	public void setGrad(Grad grad) {
		this.grad = grad;
	}

	public Fabrika getFabrikaNaAdresi() {
		return fabrikaNaAdresi;
	}

	public void setFabrikaNaAdresi(Fabrika fabrikaNaAdresi) {
		this.fabrikaNaAdresi = fabrikaNaAdresi;
	}

	public Prodavnica getProdavnicaNaAdresi() {
		return prodavnicaNaAdresi;
	}

	public void setProdavnicaNaAdresi(Prodavnica prodavnicaNaAdresi) {
		this.prodavnicaNaAdresi = prodavnicaNaAdresi;
	}

}
