package prodavnica;

import java.util.ArrayList;

public class Prodavnica {
	private String ime;
	private String tipProdavnice;
	private Adresa adresa;
	private Covek vlasnik;
	private ArrayList<Covek> zaposleni;
	private ArrayList<Proizvod> proizvodiUPonudi;

	public Prodavnica() {
		super();
	}

	public String getAPR(Boolean capitalize) {
		if (capitalize = true) {
			return capitalize(ime) + ", " + capitalize(tipProdavnice) + ", " + capitalize(adresa.getUlica()) + ", "
					+ capitalize(vlasnik.getIme()) + ", [" + zaposleni.size() + "]";
		} else {
			return ime + ", " + tipProdavnice + ", " + adresa.getUlica() + ", " + vlasnik.getIme() + ", ["
					+ zaposleni.size() + "]";
		}
	}
	
	public ArrayList<String> getImenaZaposlenih(){
		ArrayList<String>rez = new ArrayList<String>();
		for(Covek radnik : zaposleni){
			rez.add(radnik.getIme());
		}
			return rez;
	}

	private String capitalize(String ime2) {
		// TODO Auto-generated method stub
		return ime2.toUpperCase();
	}

	public ArrayList<Proizvod> getProizvodiUPonudi() {
		return proizvodiUPonudi;
	}

	public void setProizvodiUPonudi(ArrayList<Proizvod> proizvodiUPonudi) {
		this.proizvodiUPonudi = proizvodiUPonudi;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getTipProdavnice() {
		return tipProdavnice;
	}

	public void setTipProdavnice(String tipProdavnice) {
		this.tipProdavnice = tipProdavnice;
	}

	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

	public Covek getVlasnik() {
		return vlasnik;
	}

	public void setVlasnik(Covek vlasnik) {
		this.vlasnik = vlasnik;
	}

	public ArrayList<Covek> getZaposleni() {
		return zaposleni;
	}

	public void setZaposleni(ArrayList<Covek> zaposleni) {
		this.zaposleni = zaposleni;
	}
	

}
