package prodavnica;

import java.util.ArrayList;

public class Proizvod {
	private String ime;
	private Fabrika proizvodjac;
	private ArrayList<Prodavnica>prodajnaMesta;
	private String tipProizvoda;
	private Double cena;
	private Integer kolicina;
	
	public ArrayList<String>getProdajniGradovi(){
		ArrayList<String>rez = new ArrayList<String>();
		for(Prodavnica a : prodajnaMesta){
			String grad = a.getAdresa().getGrad().getIme();
			rez.add(grad);
		}
		
		return rez;
		
	}
	
	public ArrayList<String>getProdajneDrzave(){
		ArrayList<String>rez = new ArrayList<String>();
		for(Prodavnica a : prodajnaMesta){
			String drzava = a.getAdresa().getGrad().getDrzava();
			rez.add(drzava);
		}
		
		return rez;
	}
	
	public Double getUkupnaVrednost(){
		return cena * kolicina;
	}
	
	String getPotpis(){
		return ime + " proizveden u " + proizvodjac.getAdresa().getGrad().getIme();
	}


	public String getIme() {
		return ime;
	}

public void setIme(String ime) {
	this.ime = ime;
}
}
