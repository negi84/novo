package prodavnica;

import java.util.ArrayList;

public class Fabrika {
	private String ime;
	private Covek vlasnik;
	private Adresa adresa;
	private ArrayList<Proizvod>proizvodi;
	public Fabrika() {
		super();
	}
	
	public ArrayList<String> getImenaProizvoda(){
		ArrayList<String> rez = new ArrayList<String>();
		for(Proizvod pr : proizvodi){
			rez.add(pr.getIme());
		}
		return rez;
	}

	public String getPotpisVlasnika(){
		return vlasnik.getPotpis();
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Covek getVlasnik() {
		return vlasnik;
	}

	public void setVlasnik(Covek vlasnik) {
		this.vlasnik = vlasnik;
	}

	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

	public ArrayList<Proizvod> getProizvodi() {
		return proizvodi;
	}

	public void setProizvodi(ArrayList<Proizvod> proizvodi) {
		this.proizvodi = proizvodi;
	}
	
	
}
