package fakultet;

public class SaradnikUNastavi extends Asistent {
	private Double trenutniProsekNaOsnovnimStudijama;

	public SaradnikUNastavi() {}

	public Double getTrenutniProsekNaOsnovnimStudijama() {
		return trenutniProsekNaOsnovnimStudijama;
	}

	public void setTrenutniProsekNaOsnovnimStudijama(Double trenutniProsekNaOsnovnimStudijama) {
		this.trenutniProsekNaOsnovnimStudijama = trenutniProsekNaOsnovnimStudijama;
	}
	
	
	
}
