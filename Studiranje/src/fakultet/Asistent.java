package fakultet;

public class Asistent extends Nastavnik {
	private String godinaStudija;
	private String dodatnoInteresovanje;

	public Asistent() {
	}

	public Asistent(String godinaStudija, String dodatnoInteresovanje) {
		super();
		this.godinaStudija = godinaStudija;
		this.dodatnoInteresovanje = dodatnoInteresovanje;
	}

	public String getDodatnoInteresovanje() {
		return dodatnoInteresovanje;
	}

	public void setDodatnoInteresovanje(String dodatnoInteresovanje) {
		this.dodatnoInteresovanje = dodatnoInteresovanje;
	}

	public String getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(String godinaStudija) {
		this.godinaStudija = godinaStudija;
	}

}
