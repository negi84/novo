package fakultet;

import java.util.ArrayList;

public class Predmet {
	private String naziv;
	private Profesor profesor;
	private ArrayList<Asistent> asistenti;
	private ArrayList<Student> studenti;

	public Predmet() {
		super();
	}
	
	

	public Predmet(String naziv, Profesor profesor, ArrayList<Asistent> asistenti, ArrayList<Student> studenti) {
		super();
		this.naziv = naziv;
		this.profesor = profesor;
		this.asistenti = asistenti;
		this.studenti = studenti;
	}



	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

}
