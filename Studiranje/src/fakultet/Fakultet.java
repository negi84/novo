package fakultet;

import java.util.ArrayList;

public class Fakultet {
	private String ime;
	private String adresa;
	private String grad;
	private Integer brojStudenata;
	private ArrayList<Nastavnik> nastavnici;
	
	//konstruktor
	public Fakultet() {
		super();
	}

	public Fakultet(String ime, String adresa, String grad, Integer brojStudenata) {
		super();
		this.ime = ime;
		this.adresa = adresa;
		this.grad = grad;
		this.brojStudenata = brojStudenata;
		
	}
	
	

	public Fakultet(String ime, String adresa, String grad, Integer brojStudenata, ArrayList<Nastavnik> nastavnici) {
		super();
		this.ime = ime;
		this.adresa = adresa;
		this.grad = grad;
		this.brojStudenata = brojStudenata;
		this.nastavnici = nastavnici;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	@Override
	public String toString() {

		return "Naziv fakulteta je: " + ime + ".";
	}
	
	public String getNaslov(){
		return ime + " " + grad + " [" + brojStudenata + "]";
	}
	
	public ArrayList<String> getSpisakDodatnihInteresovanja(){
		
		ArrayList<String> ret = new ArrayList<String>();
		for (Nastavnik nastavnik : nastavnici) {

			if( nastavnik instanceof Asistent){
				ret.add(((Asistent) nastavnik).getDodatnoInteresovanje());
			}
		}
		return ret;
	}
	
}
