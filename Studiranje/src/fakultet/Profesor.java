package fakultet;

import java.util.ArrayList;

public class Profesor extends Nastavnik {
	private ArrayList<Nastavnik>listaAsistenata;

	public Profesor() {
		super();
	}
	
	
	
	public Profesor(ArrayList<Nastavnik> listaAsistenata) {
		super();
		this.listaAsistenata = listaAsistenata;
	}



	public ArrayList<String> getImenaAsistenata(){
		ArrayList<String> ret = new ArrayList<String>();
		for (Nastavnik nas : listaAsistenata) {
			ret.add(nas.getIme());
			
		}
		
		return ret;
	}
	
	public Double getSumaPlataAsistenata(){
		Double suma = null;
		for (Nastavnik ass : listaAsistenata) {
			suma += ass.getPlata(1);
			
		}
		
		return suma; 
	}

}
