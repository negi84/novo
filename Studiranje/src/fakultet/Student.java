package fakultet;

import java.util.ArrayList;

public class Student extends Covek {
	private String ime;
	private String prezime;
	private String smer;
	private String godinaStudija;
	private Boolean budzet;
	private ArrayList<Predmet> predmeti;

	public Student() {
		super();
	}

	
	public Student(String ime, String prezime, String smer, String godinaStudija, Boolean budzet) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.smer = smer;
		this.godinaStudija = godinaStudija;
		this.budzet = budzet;
	}


	public Student(String ime, String prezime, String smer, String godinaStudija, Boolean budzet,
			ArrayList<Predmet> predmeti) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.smer = smer;
		this.godinaStudija = godinaStudija;
		this.budzet = budzet;
		this.predmeti = predmeti;
	}

	public ArrayList<Predmet> getSvaImenaPredmetaKojiSlusa() {
		return predmeti;
	}


	public String getIme() {
		return ime;
	}


	public void setIme(String ime) {
		this.ime = ime;
	}


	public String getPrezime() {
		return prezime;
	}


	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}


	public String getSmer() {
		return smer;
	}


	public void setSmer(String smer) {
		this.smer = smer;
	}


	public String getGodinaStudija() {
		return godinaStudija;
	}


	public void setGodinaStudija(String godinaStudija) {
		this.godinaStudija = godinaStudija;
	}


	public Boolean getBudzet() {
		return budzet;
	}


	public void setBudzet(Boolean budzet) {
		this.budzet = budzet;
	}


	public ArrayList<Predmet> getPredmeti() {
		return predmeti;
	}


	public void setPredmeti(ArrayList<Predmet> predmeti) {
		this.predmeti = predmeti;
	}


	
	
	

}


