package fakultet;

import java.util.ArrayList;

public class Nastavnik extends Covek {
	private Double koeficijent;
	private Integer godineStaza;
	private Integer brojObjavljenihRadova;
	private ArrayList<Predmet> predmeti;

	public Nastavnik() {}
	
	
	
	public Nastavnik(Double koeficijent, Integer godineStaza, Integer brojObjavljenihRadova,
			ArrayList<Predmet> predmeti) {
		super();
		this.koeficijent = koeficijent;
		this.godineStaza = godineStaza;
		this.brojObjavljenihRadova = brojObjavljenihRadova;
		this.predmeti = predmeti;
	}



	public Double getPlata(Integer umnozak){
		return koeficijent * this.godineStaza + 12.9 * umnozak;
	}
	
    public ArrayList<String> getNaziviPredmeta(){
    	ArrayList<String>ret = new ArrayList<String>();
    	for (Predmet pred : predmeti) {
			ret.add(pred.getNaziv());
		}
    	return ret;
    }
}
